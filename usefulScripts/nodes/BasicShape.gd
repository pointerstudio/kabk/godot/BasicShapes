@tool
extends Node3D

func new_mesh() -> PrimitiveMesh:
	printerr("BasicShape does not have a shape, it is a base for other shapes")
	return null
	
func new_shape() -> Shape3D:
	printerr("BasicShape does not have a shape, it is a base for other shapes")
	return null

@export var use_collision : bool = false:
	set(value):
		use_collision = value
		if is_inside_tree() and has_collision() != value:
			if use_collision:
				add_collision()
			else:
				remove_collision()
		notify_property_list_changed()

@export var use_physics : bool = false:
	set(value):
		use_physics = value
		if is_inside_tree() and has_physics() != value:
			if use_physics:
				add_physics()
			else:
				remove_physics()
		notify_property_list_changed()

var mesh_instance : MeshInstance3D = null

func _validate_property(property: Dictionary):
	if property.name == "use_physics" and not use_collision:
		property.usage |= PROPERTY_USAGE_READ_ONLY

func _enter_tree():
	if Engine.is_editor_hint():
		if not has_children_of_type(self, MeshInstance3D):
			mesh_instance = MeshInstance3D.new()
			add_child(mesh_instance, true)
			mesh_instance.owner = get_tree().edited_scene_root
			mesh_instance.mesh = new_mesh()
		else:
			mesh_instance = find_children_of_type(self, MeshInstance3D)[0]
		
		if use_collision and not has_collision():
			add_collision()
			if use_physics and not has_physics():
				add_physics()
	
func has_physics(mom : Node = self) -> bool:
	return has_children_of_type(mom, RigidBody3D)
	
func has_collision(mom : Node = self) -> bool:
	return has_children_of_type(mom, CollisionShape3D)

func has_mesh(mom : Node = self) -> bool:
	return has_children_of_type(mom, MeshInstance3D)
	
func find_children_of_type(mom : Node = self, NeedleClass = Node, recursive : bool = true) -> Array[Node]:
	var out : Array[Node] = []
	for child : Node in mom.get_children():
		if is_instance_of(child, NeedleClass):
			out.append(child)
		if recursive:
			out.append_array(find_children_of_type(child, NeedleClass, recursive))
	return out
	
func has_children_of_type(mom : Node = self, NeedleClass = Node, recursive : bool = true) -> bool:
	for child : Node in mom.get_children():
		if is_instance_of(child, NeedleClass):
			return true
		if recursive and has_children_of_type(child, NeedleClass, recursive):
			return true
	return false
	
func add_collision(mom : Node = self) -> void:
	print("add collision to %s" % mom.name)
	var collision_object : CollisionObject3D
	if use_physics:
		collision_object = RigidBody3D.new()
	else:
		collision_object = StaticBody3D.new()
	
	var collision_shape = CollisionShape3D.new()
	collision_shape.shape = new_shape()
	collision_object.add_child(collision_shape, true)
	mom.remove_child(mesh_instance)
	mom.add_child(collision_object, true)
	collision_object.add_child(mesh_instance)
	collision_object.owner = get_tree().edited_scene_root
	collision_shape.owner = get_tree().edited_scene_root
	mesh_instance.owner = get_tree().edited_scene_root

func remove_collision(mom : Node = self) -> void:
	print("remove collusion to %s" % mom.name)
	for child : Node in mom.get_children():
		if is_instance_of(child, CollisionObject3D) and child.is_ancestor_of(mesh_instance):
			child.remove_child(mesh_instance)
			mom.add_child(mesh_instance, true)
			mesh_instance.owner = get_tree().edited_scene_root
			mom.remove_child(child)
			
func add_physics(mom : Node = self) -> void:
	print("add pphysics to %s" % mom.name)
	var rigid_body = RigidBody3D.new()
	mom.add_child(rigid_body, true)
	rigid_body.owner = get_tree().edited_scene_root
	
	for child : Node in mom.get_children():
		if is_instance_of(child, StaticBody3D):
			if child.is_ancestor_of(mesh_instance):
				child.remove_child(mesh_instance)
				
			for subchild in child.get_children():
				if is_instance_of(subchild, CollisionShape3D):
					child.remove_child(subchild)
					rigid_body.add_child(subchild, true)
					rigid_body.add_child(mesh_instance)
					mesh_instance.owner = get_tree().edited_scene_root
					subchild.owner = get_tree().edited_scene_root
			mom.remove_child(child)
			
func remove_physics(mom : Node = self) -> void:
	print("remove pphysics to %s" % mom.name)
	var static_body = StaticBody3D.new()
	mom.add_child(static_body, true)
	static_body.owner = get_tree().edited_scene_root
	
	for child : Node in mom.get_children():
		if is_instance_of(child, RigidBody3D):
			if child.is_ancestor_of(mesh_instance):
				child.remove_child(mesh_instance)
			for subchild in child.get_children():
				if is_instance_of(subchild, CollisionShape3D):
					child.remove_child(subchild)
					static_body.add_child(subchild, true)
					static_body.add_child(mesh_instance)
					mesh_instance.owner = get_tree().edited_scene_root
					subchild.owner = get_tree().edited_scene_root
					break
			mom.remove_child(child)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
